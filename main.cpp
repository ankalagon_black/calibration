#include <opencv2/aruco.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <iostream>
#include<fstream>

#include <rs.hpp>

#include "gui.h"

using namespace cv;
using namespace cv::aruco;
using namespace std;
using namespace rs2;

Mat frame_to_mat(const rs2::frame& f);

int main() {
    log_to_console(RS2_LOG_SEVERITY_ERROR);

    window app(640, 480, "Capture");

    texture color_image;

    Ptr<Dictionary> dictionary = getPredefinedDictionary(DICT_7X7_250);
    Ptr<DetectorParameters> parameters = DetectorParameters::create();
    Ptr<GridBoard> board = GridBoard::create(5, 7, 0.04f, 0.01f, dictionary);
    Size size(640, 480);
    Scalar detectedColor(0, 255, 255);

    vector<vector<Point2f>> allCorners, corners;
    vector<int> allIds, ids;
    vector<int> counters;

    ifstream input;
    input.open("calib_res.txt");

    Mat cameraMatrix(3, 3, CV_64F), distCoeffs(1, 5, CV_64F);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            input >> cameraMatrix.at<double>(i,j);
        }
    }

//    cout << cameraMatrix << endl;

    for (int i = 0; i < 5; ++i) {
        input >> distCoeffs.at<double>(0, i);
    }

//    cout << distCoeffs << endl;

    input.close();

    pipeline pipe;
    pipe.start();

//    app.on_key_release = [&pipe, &allCorners, &corners, &counters, &allIds, &ids, &board, &size] (int key) {
//        if(key == GLFW_KEY_S) {
//            pipe.stop();
//
//            allCorners.insert(allCorners.cend(), corners.begin(), corners.end());
//            allIds.insert(allIds.cend(), ids.begin(), ids.end());
//            counters.push_back(static_cast<int &&>(corners.size()));
//
//            pipe.start();
//
//            cout << "Set of data #" << counters.size() << " with " << corners.size() << " elements was added." << endl;
//        }
//        else if(key == GLFW_KEY_C) {
//            Mat cameraMatrix, distCoeffs;
//            cameraMatrix = Mat::eye(3, 3, CV_64F);
//            calibrateCameraAruco(allCorners, allIds, counters, board, size, cameraMatrix, distCoeffs);
//            cout << cameraMatrix << endl;
//            cout << distCoeffs << endl;
//        }
//        else if(key == GLFW_KEY_E) {
//            cout << "Calibration data set is cleared" << endl;
//            allCorners.clear();
//            allIds.clear();
//            counters.clear();
//        }
//        else if(key == GLFW_KEY_R) {
//            cout << "Last data set was deleted" << endl;
//            allCorners.pop_back();
//            ids.pop_back();
//            counters.pop_back();
//        }
//    };

    while(app) {
        frameset data = pipe.wait_for_frames();
        frame color = data.get_color_frame();

        Mat src = frame_to_mat(color);

        vector<vector<Point2f>> rejectedCandidates;
        corners.clear();
        ids.clear();

        detectMarkers(src, dictionary, corners, ids, parameters, rejectedCandidates);
        if(!corners.empty()) {
            drawDetectedMarkers(src, corners, ids, detectedColor);

            vector<Vec3d> rvec, tvec;
            estimatePoseSingleMarkers(corners, 0.04f, cameraMatrix, distCoeffs, rvec, tvec);
            for (int i = 0; i < ids.size(); ++i) {
                drawAxis(src, cameraMatrix, distCoeffs, rvec[i], tvec[i], 0.1);
            }
        }

        cvtColor(src, src, CV_RGB2BGR);

        color_image.render(color, { 0 , 0, app.width(), app.height() });
    }

    pipe.stop();

    return EXIT_SUCCESS;
}

Mat frame_to_mat(const rs2::frame& f) {
    using namespace cv;
    using namespace rs2;

    auto vf = f.as<video_frame>();
    const int w = vf.get_width();
    const int h = vf.get_height();

    if (f.get_profile().format() == RS2_FORMAT_BGR8)
    {
        return Mat(Size(w, h), CV_8UC3, (void*)f.get_data(), Mat::AUTO_STEP);
    }
    else if (f.get_profile().format() == RS2_FORMAT_RGB8)
    {
        auto r = Mat(Size(w, h), CV_8UC3, (void*)f.get_data(), Mat::AUTO_STEP);
        cvtColor(r, r, CV_RGB2BGR);
        return r;
    }
    else if (f.get_profile().format() == RS2_FORMAT_Z16)
    {
        return Mat(Size(w, h), CV_16UC1, (void*)f.get_data(), Mat::AUTO_STEP);
    }
    else if (f.get_profile().format() == RS2_FORMAT_Y8)
    {
        return Mat(Size(w, h), CV_8UC1, (void*)f.get_data(), Mat::AUTO_STEP);
    }

    throw std::runtime_error("Frame format is not supported yet!");
}